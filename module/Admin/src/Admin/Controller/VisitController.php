<?php

namespace Admin\Controller;


use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Doctrine\Tests\DoctrineTestCase;
use DoctrineModule\Paginator\Adapter\Selectable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator\Paginator;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class VisitController extends AbstractActionController
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function listAction()
    {
        $visit = $this->em->getRepository(\Application\Entity\Visit::class);

        $criteria = new Criteria();
        $criteria->orderBy(array('id' => Criteria::DESC));

        $adapter = new Selectable($visit, $criteria);

        $paginator = new Paginator($adapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
        $paginator->setItemCountPerPage(50);

        return new ViewModel(['visits' => $paginator]);
    }

    public function csvAction()
    {
        $type = $this->params()->fromQuery('type', 'visitor');

        $qb = $this->em->createQueryBuilder();

        if ($type == 'visitor') {
            $qb->select(array('v.ip','v.country'))
                ->addSelect($qb->expr()->count('v.ip') . ' as total')
                ->from(\Application\Entity\Visit::class, 'v')
                ->orderBy('total', 'DESC')
                ->groupBy('v.ip')
                ->addGroupBy('v.country');

            $file = 'visitor.csv';
        } else {
            $qb->select(array('v.country'))
                ->addSelect($qb->expr()->count('v.country') . ' as total')
                ->from(\Application\Entity\Visit::class, 'v')
                ->orderBy('total', 'DESC')
                ->groupBy('v.country');
            $file = 'country.csv';
        }

        $sql = $this->em->createQuery($qb);
        $result = $sql->getResult();

        $fp = fopen("data/{$file}", 'w');

        foreach ($result as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);

        return $this->downloadFile($file);
    }

    private function downloadFile($file) {
        $fileName = "data/{$file}";

        $response = new \Zend\Http\Response\Stream();
        $response->setStream(fopen($fileName, 'r'));
        $response->setStatusCode(200);

        $headers = new \Zend\Http\Headers();
        $headers->addHeaderLine('Content-Type', 'text/csv')
            ->addHeaderLine('Content-Disposition', 'attachment; filename="' . $file . '"')
            ->addHeaderLine('Content-Length', filesize($fileName));

        $response->setHeaders($headers);
        return $response;
    }
}