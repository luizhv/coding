<?php

namespace Admin\Controller;


use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Paginator\Adapter\Selectable;
use Zend\Crypt\Password\Bcrypt;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator\Paginator;
use Zend\View\Model\ViewModel;
use ZfcUserDoctrineORM\Entity\User;

class UserController extends AbstractActionController
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function listAction()
    {
        $user = $this->em->getRepository('ZfcUserDoctrineORM\Entity\User');
//        $users = $user->findAll();

        $criteria = new Criteria();
        $criteria->orderBy(array('id' => Criteria::DESC));

        $adapater = new Selectable($user, $criteria);
        $paginator = new Paginator($adapater);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
        $paginator->setItemCountPerPage(10);

        $view = new ViewModel(['users' => $paginator]);
        return $view;
    }

    public function registerAction()
    {
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();

            $bcrypt = new Bcrypt();
            $bcrypt->setCost(14);
            $password = $bcrypt->create($post->password);

            $user = new User();
            $user->setEmail($post->email);
            $user->setDisplayName($post->display_name);
            $user->setUsername($post->username);
            $user->setPassword($password);

            $this->em->persist($user);
            $this->em->flush();

            $this->redirect()->toRoute('admin/user');
        }

    }
}