<?php

namespace Admin\Authorization;


use Zend\Mvc\MvcEvent;

class AuthorizationListener
{
    public function __invoke(MvcEvent $mvcEvent)
    {
        $routeMatch = $mvcEvent->getRouteMatch();
        $routeName = $routeMatch->getMatchedRouteName();

        $auth = $mvcEvent->getApplication()->getServiceManager()->get('zfcuser_auth_service');
        if (!$auth->hasIdentity()) {

        }
    }
}