<?php
namespace Admin;

use BjyAuthorize\View\RedirectionStrategy;
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

class Module
{
    public function init(ModuleManager $moduleManager)
    {
        $events = $moduleManager->getEventManager();
        $sharedEvents = $events->getSharedManager();
        $sharedEvents->attach(__NAMESPACE__, MvcEvent::EVENT_DISPATCH, function(MvcEvent $e) {
            $controller = $e->getTarget();
            $view = $e->getViewModel();

            if ($view instanceof ViewModel) {
                if (!$view->terminate()) {
                    $controller->layout('layout/admin');
                }
            }
        });
    }

    public function onBootstrap(MvcEvent $mvcEvent)
    {
        $eventManager = $mvcEvent->getApplication()->getEventManager();
        $sharedEvents = $eventManager->getSharedManager();

        $strategy = new RedirectionStrategy();
        $strategy->setRedirectRoute('zfcuser/login');
        $eventManager->attach($strategy);


        $eventManager->attach(MvcEvent::EVENT_DISPATCH, function(MvcEvent $mvcEvent) {
            $route = $mvcEvent->getTarget();
            $controller = $mvcEvent->getRouteMatch();

            if ($controller->getMatchedRouteName() == 'zfcuser/login') {
                $sm = $mvcEvent->getApplication()->getServiceManager();
                $auth = $sm->get('zfcuser_auth_service');
                if (!$auth->hasIdentity()) {
                    $view = $mvcEvent->getViewModel();
                    $view->setTemplate('layout/layout-login');
                }
            }
        });
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
