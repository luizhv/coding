<?php

namespace Application\Fixture;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Fixtures;
use Zend\Crypt\Password\Bcrypt;
use ZfcUserDoctrineORM\Entity\User;

class FixtureLoader implements FixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $bcrypt = new Bcrypt();
        $bcrypt->setCost(14);
        $password = $bcrypt->create("password");

        $user = new User();
        $user->setDisplayName("Admin User");
        $user->setEmail("admin@admin.com");
        $user->setPassword($password);
        $user->setUsername("admin");
        $user->setState(1);

        $manager->persist($user);
        $manager->flush();

        $obj = Fixtures::load(__DIR__ . '/fixtures.yml', $manager);
    }
}