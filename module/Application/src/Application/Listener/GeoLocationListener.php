<?php

namespace Application\Listener;


use Application\Entity\Visit;
use Zend\Http\PhpEnvironment\RemoteAddress;
use Zend\Json\Json;
use Zend\Mvc\MvcEvent;

class GeoLocationListener
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    public function __invoke(MvcEvent $event)
    {
        $this->em = $event->getApplication()->getServiceManager()->get('doctrine.entitymanager.orm_default');

        $route = $event->getTarget();
        $controller = $event->getRouteMatch();
        $view = $event->getResult();

        switch($controller->getMatchedRouteName()) {
            case 'home';
            case 'contact';
            case 'about';
                $this->registerVisitor($controller->getMatchedRouteName());
            break;
        }
    }

    public function registerVisitor($page)
    {
        $remote = new RemoteAddress();
        $data = file_get_contents(sprintf('https://freegeoip.net/json/%s',$remote->getIpAddress()));

        $geoip = Json::decode($data);

        $country = $geoip->country_name ? : 'local';

        $visit = new Visit();
        $visit->setCountry($country);
        $visit->setDate(new \DateTime());
        $visit->setPage($page);
        $visit->setIp($remote->getIpAddress());

        $this->em->persist($visit);
        $this->em->flush();
    }
}