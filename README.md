Coding Course
=======================

Introduction
------------
Fullstack developer test

Installation
---------------------------

1. Install composer dependencies

        composer install
    
2. Run docker composer

        docker-composer up -d
    
3. Open php container bash

        docker exec -it course_php_1 bash
        
4. Run doctrine migrations to create database and migrations

        ./vendor/bin/doctrine-module migrations:diff
        ./vendor/bin/doctrine-module migrations:migrate
        
5. Run data fixtures create user and load fake data

        ./vendor/bin/doctrine-module data-fixture:import
        
6. Access http://localhost:8080

Admin
---------------------------

1. Access http://localhost:8080/admin

        user: admin@admin.com
        password: password